from dataclasses import dataclass
from typing import Any


@dataclass
class MiddlemanAlert:
    timestamp: str
    host: str
    type: str
    domain: str


def format_alert(row: Any) -> MiddlemanAlert:
    timestamp_row = row["timestamp"]
    timestamp_isoformat = timestamp_row.isoformat()
    return MiddlemanAlert(
        timestamp=timestamp_isoformat,
        host=row["src_ip"],
        type="dga",
        domain=row["dns.rrname"],
    )
