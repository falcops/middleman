import logging
import os

import redis
from eeland.utils import bootstrap_elastic_client

from middle.man import Man
from middle.utils import bootstrap_kafka_producer

logging.basicConfig(
    format="middleman %(asctime)s - %(levelname)s: %(message)s",
    level=os.environ.get(key="LOG_LEVEL", default="INFO").upper(),
)


def main(
    to_kafka: bool = True,
    redis_cache: bool = False,
    to_stdout: bool = False,
) -> None:
    uri = os.environ["MIDDLEMAN_SHADOWSERVER_URI"]
    key = os.environ["MIDDLEMAN_SHADOWSERVER_KEY"]
    secret = os.environ["MIDDLEMAN_SHADOWSERVER_SECRET"]
    es_client = bootstrap_elastic_client(
        es_host=os.environ.get("ES_HOST"),
        es_user=os.environ.get("ES_USER"),
        es_password=os.environ.get("ES_PASSWORD"),
        use_ssl=True,
    )
    begin_epoch = float(os.environ.get("BEGIN_EPOCH", 1609459200.0))
    batch_size_seconds = int(os.environ.get("BATCH_SIZE_SECONDS", 3600))
    if to_kafka:
        kafka_producer = bootstrap_kafka_producer(
            kafka_host=os.environ["KAFKA_HOST"], kafka_port=os.environ["KAFKA_PORT"]
        )
    else:
        kafka_producer = None
    if redis_cache:
        redis_client = redis.Redis(
            host=os.getenv("REDIS_HOST", "redis"),
            port=os.getenv("REDIS_PORT", 6379),  # type: ignore
            db=0,
        )
    else:
        redis_client = None  # type: ignore
    middleman = Man(
        api_uri=uri,
        api_key=key,
        api_secret=secret,
        es_client=es_client,
        es_index=os.environ.get("MIDDLEMAN_DNS_ELASTIC_INDEX", "dns*"),
        kafka_producer=kafka_producer,
        redis_client=redis_client,
        to_stdout=to_stdout,
    )
    middleman.catchup_and_stalk(begin_epoch, batch_size_seconds)


if __name__ == "__main__":
    main()
