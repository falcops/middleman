import hashlib
import hmac
import json
import logging
import socket
import time
from dataclasses import asdict
from datetime import datetime
from distutils.util import strtobool
from typing import Any

import pandas as pd
import requests
from eland import eland_to_pandas
from elasticsearch import Elasticsearch, exceptions
from kafka.producer.kafka import KafkaProducer
from redis import Redis

from middle.alert import format_alert
from middle.utils import get_domains_dataframe_from_elastic


class Man:
    def __init__(
        self,
        api_uri: str,
        api_key: str,
        api_secret: str,
        es_client: Elasticsearch,
        kafka_producer: KafkaProducer,
        redis_client: Redis,  # type: ignore
        to_stdout: bool,
        kafka_topic: str = "raw_middleman",
        es_index: str = "dns*",
        delay: int = 0,
    ) -> None:
        self.api_uri = api_uri
        self.api_key = api_key
        self.api_secret = api_secret
        self.es_client = es_client
        self.es_index = es_index
        self.kafka_producer = kafka_producer
        self.kafka_topic = kafka_topic
        self.redis_client = redis_client
        self.delay = delay
        self.to_stdout = to_stdout
        self.hot_domains = self.retrieve_tno_export_domains()

    def encode_body(self, request_body):
        request_body["apikey"] = self.api_key
        request_string = json.dumps(request_body)
        return bytes(request_string, "latin-1")

    def compute_hmac2(self, request_bytes):
        secret_bytes = bytes(self.api_secret, "latin-1")
        hmac_generator = hmac.new(secret_bytes, request_bytes, hashlib.sha256)
        hmac2_digest = hmac_generator.hexdigest()
        return hmac2_digest

    def prepare_headers(self, encoded_body):
        hmac2_digest = self.compute_hmac2(request_bytes=encoded_body)
        headers = {"HMAC2": hmac2_digest}
        return headers

    def retrieve_tno_export_domains(
        self,
        limit: int = 0,
        exclude_known_dga: bool = False,
        exclude_reconyc: bool = False,
    ) -> pd.Series:
        """
        retrieves a report with the last 24h of domains classified by dga detective
        """
        logging.info("pulling latest data from research/tno-export")
        url = self.api_uri + "research/tno-export"
        request_body = {}
        if limit:
            request_body["limit"] = limit
        if exclude_known_dga:
            request_body["skip"].append("rpz-dga")  # type: ignore
        if exclude_reconyc:
            request_body["skip"].append("noise")  # type: ignore
        encoded_body = self.encode_body(request_body=request_body)
        headers = self.prepare_headers(encoded_body=encoded_body)
        response = requests.post(url, headers=headers, data=encoded_body, verify=False)
        df = pd.read_json(response.text, lines=True)
        logging.info(f"pulled {len(df)} domains from research/tno-export")
        return list(df.domain.values)

    def is_domain_cold(self, domain: str) -> bool:
        """
        checks if domain is in shadowserver historic dataset and returns True if
        domain has been tagged as DGA by dga detective, otherwise it returns False
        """
        endpoint = "research/malware-domain"
        request_body = {"domain": domain}
        response = None
        attempts = 0
        while not response:
            attempts += 1
            try:
                response = self.query_api(endpoint, request_body)
            except socket.timeout:
                logging.error(
                    f"timeout no response for {domain} - retrying... attempt #{attempts}"
                )
        response_dict = json.loads(response)
        try:
            tags = response_dict[domain]["tag"]
        except KeyError:
            is_dga = False
        else:
            is_dga = "tno-dga-tagged" in tags
        finally:
            time.sleep(self.delay)
            return is_dga

    def is_domain_hot(self, domain: str) -> bool:
        # TODO: use string grouper to get almost exact matches
        # TODO: strip tld and www?
        return domain in self.hot_domains

    def check_domain(
        self,
        row: Any,
        check_cold: bool = False,
    ) -> None:
        # sourcery skip: assign-if-exp, or-if-exp-identity
        domain = row["dns.rrname"]
        # check in cache (redis) if we already have a result
        from_cache = False
        start_timer = time.time()
        cached_result = self.redis_client.get(domain) if self.redis_client else None
        if cached_result:
            is_malicious = bool(strtobool(cached_result.decode("utf-8")))
            from_cache = True
        # if we don't or if cache is disabled, we query the api
        else:
            is_hot = self.is_domain_hot(domain)
            is_cold = self.is_domain_cold(domain) if check_cold else False
            is_malicious = is_hot or is_cold
            if self.redis_client:
                self.redis_client.set(domain, str(is_malicious))

        # measure elapsed time
        end_timer = time.time()
        elapsed = round(end_timer - start_timer, 2)
        logging.debug(
            f"{domain}: malicious={is_malicious}, from_cache={from_cache}, took={elapsed}s"
        )
        alert = format_alert(row=row)
        if is_malicious:
            if self.kafka_producer:
                logging.debug(asdict(alert))
                self.kafka_producer.send(self.kafka_topic, asdict(alert))

    def check_domains(
        self,
        from_elastic: bool = True,
        start_epoch: float = 0,
        end_epoch: float = datetime.now().timestamp(),
        fetch_hot_domains: bool = True,
    ) -> None:
        """
        1. pulls domains from elasticsearch with eland
        2. check each domain
        3. if malicious, send alert to kafka/stdout
        """
        if fetch_hot_domains:
            self.retrieve_tno_export_domains()
        if from_elastic:
            dataframe = get_domains_dataframe_from_elastic(
                es_client=self.es_client,
                es_index=self.es_index,
                start_epoch=start_epoch,
                end_epoch=end_epoch,
            )
            dataframe = eland_to_pandas(dataframe)
            for _, row in dataframe.iterrows():
                try:
                    self.check_domain(row)
                except exceptions.NotFoundError as e:
                    logging.error(e)
                    break

    def catchup_backlog(
        self, begin_epoch: float = 1609459200.0, batch_size_seconds: int = 3600
    ) -> float:
        """
        efficiently analyse batches of data from begin_epoch to now
        1609459200.0 = Fri Jan 01 2021 00:00:00 GMT+0000
        """
        now = int(datetime.now().timestamp())
        epochs = now - begin_epoch
        amount_batches = int(epochs / batch_size_seconds)
        logging.critical("catching up with backlog")
        for batch in range(amount_batches):
            logging.critical(f"backlog batch #{batch+1}/{amount_batches}")
            start_epoch = begin_epoch + batch_size_seconds * batch
            end_epoch = min(
                start_epoch + batch_size_seconds, datetime.now().timestamp()
            )
            self.check_domains(
                start_epoch=start_epoch, end_epoch=end_epoch, fetch_hot_domains=True
            )
        return end_epoch

    def real_time_analysis(self, start_epoch: float) -> None:
        """
        analyise data in semi real time
        """
        logging.critical("start real time analysis")
        while True:
            end_epoch = datetime.now().timestamp()
            self.check_domains(
                start_epoch=start_epoch, end_epoch=end_epoch, fetch_hot_domains=True
            )

    def catchup_and_stalk(self, begin_epoch: float, batch_size_seconds: int) -> None:
        end_epoch_last_batch = self.catchup_backlog(begin_epoch, batch_size_seconds)
        self.real_time_analysis(start_epoch=end_epoch_last_batch)
