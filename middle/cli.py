import click

from middle.main import main


@click.command()
@click.option(
    "--to-kafka",
    default=True,
    help="to send alerts to kafka",
    show_default=True,
)
@click.option(
    "--redis-cache",
    default=True,
    help="enables redis caching",
    show_default=True,
)
@click.option(
    "--to-stdout",
    default=True,
    help="sends kafka alerts to stdout",
    show_default=True,
)
def mman_cli(to_kafka: bool, redis_cache: bool, to_stdout: bool) -> None:
    main(to_kafka, redis_cache, to_stdout)
