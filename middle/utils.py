import json
import logging
import os
import time
from datetime import datetime
from typing import Optional

import eeland
import eland
from elasticsearch import Elasticsearch
from kafka import KafkaProducer
from kafka.errors import NoBrokersAvailable


def get_domains_dataframe_from_elastic(
    es_client: Elasticsearch,
    es_index: str = "dns",
    start_epoch: float = 0.0,
    end_epoch: float = datetime.now().timestamp(),
) -> eland.DataFrame:
    dataframe = eland.DataFrame(es_client, es_index)
    dataframe = eeland.utils.range_query(
        dataframe=dataframe,
        range_field="timestamp",
        start_range=start_epoch,
        end_range=end_epoch,
    )
    dataframe = dataframe[dataframe["dns.type"] == "query"]
    dataframe = dataframe[["timestamp", "src_ip", "dns.rrname"]]
    return dataframe


def bootstrap_kafka_producer(
    kafka_host: str, kafka_port: int = 9092, max_attempts: int = 5
) -> Optional[KafkaProducer]:
    connected = False
    attempts_count = 0
    while not connected and attempts_count < max_attempts:
        try:
            attempts_count += 1
            kafka_producer = KafkaProducer(
                bootstrap_servers=[f"{kafka_host}:{kafka_port}"],
                value_serializer=lambda v: json.dumps(v).encode("utf-8"),
                acks="all",
            )
        except (NoBrokersAvailable, ConnectionResetError):
            logging.warning(
                "can't reach kafka at %s:%s (attempt #%s), retrying...",
                kafka_host,
                kafka_port,
                attempts_count,
            )
            time.sleep(1)
            kafka_producer = None
        else:
            logging.critical("connected to kafka at %s:%s", kafka_host, kafka_port)
            connected = True
        finally:
            if attempts_count == max_attempts:
                logging.critical(
                    "could not connect to kafka after %s attempts, stopping...",
                    attempts_count,
                )
    return kafka_producer
