# AAD - shadowserver middleman

1. pulls domains from an elasticsearch index that uses the suricata dns log format
2. pulls list of _fresh_ (classified as dga in the last 24h) dga domains from Shadowserver API
3. if a domain is in both lists, it sends a message to kafka at topic `raw_middleman` with the format
    * `timestamp`: timestamp of dns query
    * `host`: host that performed the dns query
    * `type`: for now only `dga`
    * `domain`: the domain classified as `dga`

## Docker swarm deployment

1. get an API `key` and `secret` from Shadowserver
2. put them in your config directory in the file `middleman.secrets.env`
3. deploy with
```bash
make middleman
```

### Dealing with large amount of data

Middleman by default comes with a docker memory limit of 2GB. By default it will pull batches of 1 hour of dns records.
Depending on the infrastructure, this may be > 2GB. If the middleman shuts down with an error `task: non-zero exit (137)`, this is why.

You can easily alter the memory requirement/limits like this:
```bash
# double requirements/limit to 4GB
docker service update middle_man --limit-memory 4GB --reserve-memory 4GB
```

Alternatively, you can reduce the size of a batch to less then 1 hour using the environment variable `BATCH_SIZE_SECONDS`
```bash
# you must use the `--force` option for the change to have an effect (middleman will restart)
docker service update middle_man --env-add BATCH_SIZE_SECONDS=1800 --force
```

You can also combine everything in one command:
```bash
docker service update middle_man --limit-memory 4GB --reserve-memory 4GB --env-add BATCH_SIZE_SECONDS=1800 --force
```

### Aknowledgement

Middleman is developed by TNO in the SOCCRATES innovation project. SOCCRATES has received funding from the European Union’s Horizon 2020 Research and Innovation program under Grant Agreement No. 833481.
