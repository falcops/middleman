import os

from middle.man import Man


def test_research_tno_export():
    uri = os.environ["MIDDLEMAN_SHADOWSERVER_URI"]
    key = os.environ["MIDDLEMAN_SHADOWSERVER_KEY"]
    secret = os.environ["MIDDLEMAN_SHADOWSERVER_SECRET"]
    middleman = Man(
        api_uri=uri,
        api_key=key,
        api_secret=secret,
        es_client=None,
        kafka_producer=None,
        redis_client=None,
        to_stdout=True,
    )
    assert len(middleman.hot_domains) > 0
