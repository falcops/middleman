FROM python:3.9-slim

WORKDIR /project
COPY middle middle
COPY pyproject.toml poetry.lock ./

RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

ENTRYPOINT [ "mman" ]
